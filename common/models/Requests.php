<?php

namespace common\models;
/**
 * This is the model class for table "requests".
 *
 * @property int $id
 * @property string $email
 * @property int $experience
 * @property string $comment
 * @property  $created_at
 */
class Requests extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'requests';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Почта',
            'experience' => 'Опыт игры',
            'comment' => 'Комментарий',
            'created_at' => 'Дата'
        ];
    }

    public function rules()
    {
        return [
            [['email', 'experience', 'comment'], 'required', 'message' => 'Это обязательное поле'],
            [['experience',], 'integer', 'min' => 1, 'max' => 3],
            [['email'], 'string', 'max' => 100],
            [['email'], 'email', 'message' => 'Введите правильный email'],
            [['email'], 'unique', 'message' => 'Такая почта уже зарегистрирована'],
            [['comment'], 'string', 'max' => 1000]
        ];
    }

    public static function experience(int $value) {
        $data = [
             1 => 'Менее одного года',
             2 => 'Более одного года',
             3 => 'Более 3 лет',
        ];
        return $data[$value];
    }
}