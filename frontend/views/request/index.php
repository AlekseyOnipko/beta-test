<?php

use common\models\Requests;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RequestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requests-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= \kartik\export\ExportMenu::widget([
            'dataProvider' => $dataProvider,
             'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'id',
            'email',
            [
                'attribute' => 'experience',
                'label' => 'Опыт',
                'value' => function($model, $key, $index, $widget) {
                    return Requests::experience($model->experience);
                }
                ],
                'comment',
                'created_at'

        ],
             'dropdownOptions' => [
                     'label' => 'Выгрузить',
                 'class' => 'btn btn-outline-secondary btn-default ',
             ]
    ])

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            [
        'attribute' => 'experience',
        'value' => function ($data) { return Requests::experience($data->experience); },
    ],

            'comment:ntext',
            'created_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Requests $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 },
                'template' => '{view}',
            ],
        ],
    ]); ?>


</div>
