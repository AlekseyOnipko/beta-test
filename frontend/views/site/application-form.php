<?php

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inknut+Antiqua&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/application-form.css">
        <title>Akeron online</title>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <main>
        <div class="main__form">
            <div class="main__form-inner">
                <a class="main__form-logo">
                    <img src="/img/logo-akeron.png" alt="logo">
                </a>

                <section class="heading__text">
                    <div class="heading__text-inner">
                        <h2 class="heading__text-title">Заполните заявку для участия <br> в Закрытом Бета Тестировании
                            TERA™</h2>
                    </div>
                </section>

                <div class="registration">
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'registration__form'
                        ]
                    ]); ?>

                    <?= $form->field($model, 'email')->input('email',
                        ['placeholder' => "Введите Ваш действующий email",
                            'class' => 'registration__form-email'],
                    )->label(false) ?>


                    <div class="experience">
                        <p class="experience__item-text">Опыт игры <br> в TERA™</p>

                        <div class="form_radio">
                            <?= $form->field($model, 'experience')->radioList([
                                1 => 'Менее 1 года',
                                2 => 'Более 1 года',
                                3 => 'Больше 3 лет',
                            ], [
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    return
                                        '<div class="form_radio_btn"><input id="radio-' . $value . '" type="radio" name="' . $name . '" value="' . $value . '" checked="' . $checked . '"><label for="radio-' . $value . '">' . $label . '</label></div>';
                                },
                            ])->label(false) ?>
                        </div>
                    </div>

                    <div class="textarea__inner">
                        <div class="form__textarea">

                                <?= $form->field($model, 'comment')->textarea([
                                    'rows' => '5',
                                    'cols' => '42',
                                    'placeholder' => "Почему именно вы должны участвовать в ЗБТ?(Пару слов о ваших целях, игровом опыте, членстве в Гильдиях и компетенциях)",
                                    'class' => 'form__textarea-palceholder',
                                ])->label(false) ?>

                                <?= \yii\helpers\Html::submitButton('принять участие в збт', ['class' => 'form__textarea-submit']) ?>
                        </div>

                    </div>

                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </main>

    <footer class="footer">
        <div class="footer__inner">

            <a class="footer__logo">
                <span class="footer__link">tera-online.su</span>
                <img src="/img/tera_online_logo.png" alt="logo">
            </a>

            <div class="footer__copyright">
                <p class="footer__copyright-text">Tera-online.su - Классический игровой проект, знаменитой онлайн игры
                    TERA™
                    TERA-ONLINE.SU | Copyright © 2021 | По всем вопросам: info@tera-online.su
                    TERA™, «TERA: The Next» and «TERA: The Battle For The New World» are trademarks of KRAFTON, Inc.
                    Copyright © 2008-2021 KRAFTON, Inc. All rights reserved.</p>
            </div>
        </div>
    </footer>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage();