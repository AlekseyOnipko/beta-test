<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Inknut+Antiqua&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/style.css">
  <title>Akeron online</title>
</head>
<body>
  <main>
    <div class="main__logo">
      <img class="main__logo-img" src="/img/logo-akeron.png" alt="logo">
    </div>

    <section class="main__text">
      <h1 class="main__title">COMING SOON <sup class="main__suptitle">*скоро</sup></h1>
    </section>

    <section class="participate">
      <a href="/form" class="participate__inner">
        <h2 class="participate__title">принять участие в збт</h2>
      </a>
    </section>

    <div class="social">
      <div class="social__links">

        <div class="social__vk">
          <a class="social__link-vk" target= "_blank" href="https://vk.com/akeron.online">Вконтакте</a>
          <img src="/img/VK_Logo.png" alt="vk">
        </div>

        <div class="social__discord">
          <img class="social__discord-img" src="/img/discord.png" alt="discord">
          <a class="social__link-discord" target= "_blank" href="https://discord.gg/VsZJcaEfcs">Discord</a>
        </div>

      </div>
    </div>
  </main>

  <footer class="footer">
    <div class="footer__inner">

      <a class="footer__logo">
        <span class="footer__link">tera-online.su</span>
        <img src="/img/tera_online_logo.png" alt="logo">
      </a>

      <div class="footer__copyright">
        <p class="footer__copyright-text">Tera-online.su - Классический игровой проект, знаменитой онлайн игры TERA™
          TERA-ONLINE.SU | Copyright © 2021 | По всем вопросам: info@tera-online.su
          TERA™, «TERA: The Next» and «TERA: The Battle For The New World» are trademarks of KRAFTON, Inc.
          Copyright © 2008-2021 KRAFTON, Inc. All rights reserved.</p>
      </div>
    </div>
  </footer>
</body>
</html>