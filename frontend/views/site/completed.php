<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css2?family=Inknut+Antiqua&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/completed.css">
  <title>Document</title>
</head>
<body>
  <main>
    <div class="main__form">
      <div class="main__form-inner">
        <a class="main__form-logo">
          <img src="/img/logo-akeron.png" alt="logo">
        </a>

        <section class="heading__text">
          <div class="heading__text-inner">
            <h2 class="heading__text-title">ВАША ЗАЯВКА ПРИНЯТА!</h2>
            <p class="heading__text-descr">Благодарим вас за интерес и небезразличие проявленные к русскоязычной TERA™.</p>
            <p class="heading__text-descr">Мы внимательно ознакомимся со всеми анкетами и 
              вышлем всю необходимую информацию для участия в Закрытом Бета Тесте 
              заинтересовавшим нас кандидатам, в ближайшее время. </p>
              < class="heading__text-info"> 
            <p class="heading__text-descr">Рекомендуем подписаться на новости в наших сообществах, зарегистрироваться
                и принимать активноек участие на форуме, чтобы быть в курсе последних новостей!)</p>
          </div>
        </section>

        <div class="social">
          <div class="social__links">
    
            <div class="social__vk">
              <a class="social__link-vk" target= "_blank" href="https://vk.com/akeron.online">Вконтакте</a>
              <img src="/img/VK_Logo.png" alt="vk">
            </div>
    
            <div class="social__discord">
              <img class="social__discord-img" target= "_blank" src="/img/discord.png" alt="discord">
              <a class="social__link-discord" href="https://discord.gg/VsZJcaEfcs">Discord</a>
            </div>
    
          </div>
        </div>
      </div>
    </div>
  </main>

  <footer class="footer">
    <div class="footer__inner">

      <a class="footer__logo">
        <span class="footer__link">tera-online.su</span>
        <img src="/img/tera_online_logo.png" alt="logo">
      </a>

      <div class="footer__copyright">
        <p class="footer__copyright-text">Tera-online.su - Классический игровой проект, знаменитой онлайн игры TERA™
          TERA-ONLINE.SU | Copyright © 2021 | По всем вопросам: info@tera-online.su
          TERA™, «TERA: The Next» and «TERA: The Battle For The New World» are trademarks of KRAFTON, Inc.
          Copyright © 2008-2021 KRAFTON, Inc. All rights reserved.</p>
      </div>
    </div>
  </footer>
</body>
</html>